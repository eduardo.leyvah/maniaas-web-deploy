/*jshint esversion: 6 */

function login(usu, pass) {
  if (usu == "" || pass == "") {
    swal({
      title: "Campo vacío",
      text: "Favor de llenar todos los campos",
      icon: "error",
      timer: 3500,
      buttons: false,
    });
  } else {
    $.when(
      $.ajax({
        method: "GET",
        url:
          "https://us-central1-tesismaniaas.cloudfunctions.net/cifrar?dato=" +
          usu,
        success: function (data) {
          usu = data;
        },
      }),
      $.ajax({
        method: "GET",
        url:
          "https://us-central1-tesismaniaas.cloudfunctions.net/cifrar?dato=" +
          pass,
        success: function (data) {
          pass = data;
        },
      })
    ).then(function (usu, pass) {
      $.ajax({
        method: "POST",
        url:
          "https://us-central1-tesismaniaas.cloudfunctions.net/login?usuario=" +
          usu[0] +
          "&passwd=" +
          pass[0],
        success: function (data) {
          if (data == "False") {
            errorLogin();
          } else {
            crearCookie(usu[0]);
            redirectMostrar();
          }
        },
      });
    });
  }
}

function redirectMostrar() {
  window.location = "admin-vm";
}

function errorLogin() {
  swal({
    title: "Error ",
    text: "El usuario o contraseña no incorrectos",
    icon: "error",
    timer: 3500,
    buttons: false,
  });
  document.getElementById("user").value = "";
  document.getElementById("password").value = "";
}

/*function cifrarDatosLogin(usu, pass) {
  if (usu == "" || pass == "") {
    swal({
      title: "Campo vacío",
      text: "Favor de llenar todos los campos",
      icon: "error",
      timer: 3500,
      buttons: false
    });
  } else {
    $.ajax({
      method: "GET",
      url:
        "https://us-central1-tesismaniaas.cloudfunctions.net/cifrar?dato=" +
        usu,
      success: function(data) {
        usu = data;
      }
    });

    $.ajax({
      method: "GET",
      url:
        "https://us-central1-tesismaniaas.cloudfunctions.net/cifrar?dato=" +
        pass,
      success: function(data) {
        pass = data;
      }
    });
    //login(usu, pass);
    //setTimeout(() => login(usu, pass), 4250);
    $.when(cifrarDatosLogin()).then(login(usu, pass));
  }
}*/

function crearCookie(nom) {
  document.cookie =
    "nombreUsuario=" + encodeURIComponent(nom) + "; path=/maniaas";
}

function eliminarCookie() {
  document.cookie = "nombreUsuario=; max-age=0; path=/maniaas";
}

function verificarCookie() {
  var resultado = "False";
  var user = document.cookie;
  if (user != "") {
    resultado = "True";
  }
  return resultado;
}
