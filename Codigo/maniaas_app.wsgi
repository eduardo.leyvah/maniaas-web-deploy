#! /usr/bin/python3.6

from maniaas_app import app as application
import logging
import sys
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, '/var/www/html/maniaas-web/')
application.secret_key = 'anything you wish'
